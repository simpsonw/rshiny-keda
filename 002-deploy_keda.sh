#!/bin/bash
helm install keda kedacore/keda --namespace ingress-nginx
echo "Done!  Run 'kubectl get pods -n ingress-nginx' to verify that the keda-oprator is running"
