# R Shiny Example Using KEDA

This repository contains a basic example of deploying an example R Shiny app (https://shiny.rstudio.com/gallery/word-cloud.html) to
Kubernetes and scale it using KEDA.  Here's how to do it:

## Prerequisites
* A Google Cloud account
* The `gcloud` CLI installed on your machine
* [Helm](https://helm.sh/) (version 3)

## Steps
1. Create a Google Cloud Project.
2. Copy `.env.default` to `.env` and provide the name of the project from the previous step and supply a cluster name and a [zone for the cluster](https://cloud.google.com/compute/docs/regions-zones).
3. Run `./000-create_keda_gcloud_clouster.sh` (this will provision a GKE cluster and install an nginx ingress in the `ingress-nginx` namespace).  Once the script completes, follow the post installation instructions.
4. Run `./001-prometheus-deployment.sh` Once the script completes, follow the post installation instructions.
5. Run `./002-deploy-keda.sh` Once the script completes, follow the post installation instructions.
6. Apply all the YAML files to your Kubernetes cluster: `kubectl apply -Rf .`
7. Get the external IP for your `nginx-ingress` service: `kubectl get svc -n ingress-nginx ingress-nginx -o jsonpath='{.status.loadBalancer.ingress[0].ip}'`
8. Add an entry to `/etc/hosts` with IP from the previous step and the hostname `rshiny.example.com`
9. Visit rshiny.example.com in a browser.  You should see the Wordcloud app.
10. (Optional) test the scalabality of the Deployment.  Use an HTTP benchmark like [hey](https://github.com/rakyll/hey) to send a bunch of requests to `http://rshiny.example.com`.  Then watch the number of pods scale up and back down (`kubectl get po -n ingress-nginx`).


## How does this all work?
We're using [Prometheus](https://prometheus.io/) to gather metrics about the R Shiny Wordcloud app.  The KEDA scaled object is uses a PromQL query as a trigger mechanism to decide when to scale (see `scaled_object.yaml` for the actual query).
