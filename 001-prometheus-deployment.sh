#!/bin/bash
kubectl apply --kustomize github.com/kubernetes/ingress-nginx/deploy/prometheus/
echo "Done!  Run  'kubectl -n ingress-nginx get svc' to verify that prometheus-server is running"
