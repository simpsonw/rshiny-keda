#!/bin/bash

if [ ! -f .env ]; then
    echo "Could not find .env.  Exiting."
    exit 1
fi

source .env


if [[ -z "${GCLOUD_PROJECT}" ]]; then
    echo "GCLOUD_PROJECT needs to be set"
    exit 1
fi

if [[ -z "${GCLOUD_CLUSTER_NAME}" ]]; then
    echo "GCLOUD_CLUSTER_NAME needs to be set"
    exit 1
fi

if [[ -z "${GCLOUD_CLUSTER_ZONE}" ]]; then
    echo "GCLOUD_CLUSTER_ZONE needs to be set"
    exit 1
fi

echo "Setting gcloud default project to $GCLOUD_PROJECT..."
gcloud config set core/project $GCLOUD_PROJECT

echo "Enabling necessary APIs..."
gcloud services enable \
       cloudapis.googleapis.com \
       container.googleapis.com \
       containerregistry.googleapis.com

echo "Provisioning cluster $GCLOUD_CLUSTER_NAME in zone $GCLOUD_CLUSTER_ZONE with preemptible VMs..."
gcloud beta container clusters create $GCLOUD_CLUSTER_NAME \
       --addons=HorizontalPodAutoscaling \
       --machine-type=g1-small \
       --cluster-version=latest --zone=$GCLOUD_CLUSTER_ZONE \
       --enable-stackdriver-kubernetes --enable-ip-alias \
       --enable-autoscaling --min-nodes=1 --max-nodes=3 \
       --enable-autorepair \
       --scopes cloud-platform \
       --preemptible

echo "Creating namespace ingress-nginx..."
kubectl create namespace ingress-nginx

sleep 10
echo "Installing nginx ingress..."
helm install ingress-controller stable/nginx-ingress \
     --namespace ingress-nginx \
     --set controller.replicaCount=2 \
     --set controller.metrics.enabled=true \
     --set controller.podAnnotations."prometheus\.io/scrape"="true" \
     --set controller.podAnnotations."prometheus\.io/port"="10254"

echo "Run  kubectl -n ingress-nginx get svc.  Your output should look something like:"
echo "NAME                                                  TYPE           CLUSTER-IP     EXTERNAL-IP     PORT(S)                      AGE"
echo "ingress-controller-nginx-ingress-controller           LoadBalancer   10.0.14.166    40.70.230.xxx   80:31036/TCP,443:32179/TCP   31s"
echo "ingress-controller-nginx-ingress-controller-metrics   ClusterIP      10.0.240.199   <none>          9913/TCP                     31s"
echo "ingress-controller-nginx-ingress-default-backend      ClusterIP      10.0.63.133    <none>          80/TCP                       31s"

echo "Then run:"
echo "kubectl apply -f https://raw.githubusercontent.com/kubernetes/ingress-nginx/nginx-0.27.1/deploy/static/mandatory.yaml"
echo "kubectl apply -f https://raw.githubusercontent.com/kubernetes/ingress-nginx/nginx-0.27.1/deploy/static/provider/cloud-generic.yaml"

